//code generate automatic not edit date: 2023-11-22T00:48:16.194Z
import dotenv from "dotenv";
import Subscribe from "onbbu/subscribe";

import ckrhqcrdxmcmblt from "@api/ckrhqcrdxmcmblt";

dotenv.config();

const subscribe: Subscribe = new Subscribe();

subscribe.use("ckrhqcrdxmcmblt::destroy", async (instance: { id: number }) => {

  console.log("hello!");
	
  await ckrhqcrdxmcmblt.destroy({
    where: {
      id: [instance.id]
    }
  });
});

const start = async (): Promise<void> => subscribe.run();

const stop = async (): Promise<void> => subscribe.stop();

export default { start, stop };