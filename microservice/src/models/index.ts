//code generate automatic not edit date: 2023-11-22T00:48:16.183Z
import Models, { DataTypes, S } from "onbbu/models";
import * as T from "../types";

const model: Models<Partial<T.ModelAttributes>> = new Models<Partial<T.ModelAttributes>>(T.name);

model.define({

  attributes: {

    fullName: DataTypes.STRING,

    age: DataTypes.BIGINT,

  },
  options: { freezeTableName: true }
});

export const sync = (props: S.SyncOptions) => model.sync(props);

export default model;