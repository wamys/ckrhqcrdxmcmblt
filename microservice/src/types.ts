//code generate automatic not edit date: 2023-11-22T00:23:37.033Z
//@onbbu-name: demo
export const name = "ckrhqcrdxmcmblt";

export interface ModelAttributes {
	id: number

	fullName: string
	age: number

	createdAt: string
	updatedAt: string
}

export interface Create {
	fullName: string
	age: number
}

export interface Update {
	where: {
		id: number
	}
	params: {
		fullName?: string
		age?: number
	}
}

export interface Destroy {
	where: {
		id: number[]
	}
}

export interface FindAndCount {
	paginate: {
		limit?: number
		offset?: number
	}
	where: {

	}
}



export const endpoint = [
  "create",
  "update",
  "destroy",
  "findAndCount",
] as const;

export type Endpoint = typeof endpoint[number]