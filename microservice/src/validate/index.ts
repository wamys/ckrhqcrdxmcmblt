//code generate automatic not edit date: 2023-11-22T00:48:16.189Z
import Joi from "onbbu/validate";
import * as T from "../types";

export const create = async (params: T.Create): Promise<T.Create> => {

  const schema = Joi.object({
    fullName: Joi.string().max(255).required(),
    age: Joi.number().min(0).required(),
  });

  await schema.validateAsync(params);

  return params;
};

export const update = async (params: T.Update): Promise<T.Update> => {

  const schema = Joi.object({
    where: Joi.object({
      id: Joi.number().min(0).required(),
    }).required(),
    params: Joi.object({
      fullName: Joi.string().max(255),
      age: Joi.number().min(0),
    }).required(),
  });

  await schema.validateAsync(params);

  return params;
};

export const destroy = async (params: T.Destroy): Promise<T.Destroy> => {

  const schema = Joi.object({
    where: Joi.object({
      id: Joi.array().items(Joi.number().min(0).required().required()).min(1).required(),
    }).required(),
  });

  await schema.validateAsync(params);

  return params;
};

export const findAndCount = async (params: T.FindAndCount): Promise<T.FindAndCount> => {

  const schema = Joi.object({
    paginate: Joi.object({
      limit: Joi.number().min(0),
      offset: Joi.number().min(0),
    }).required(),
    where: Joi.object({
    }).required(),
  });

  await schema.validateAsync(params);

  return params;
};