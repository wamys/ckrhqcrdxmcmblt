onbbu --generate service
onbbu --generate model

onbbu --auto-import
onbbu --link-type

onbbu --link-type --auto-import --pull-dependencies --registry-token <token>