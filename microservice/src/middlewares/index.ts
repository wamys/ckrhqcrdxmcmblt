//code generate automatic not edit date: 2023-11-22T00:48:16.191Z
import * as T from "../types";

export const create = async (params: T.Create): Promise<T.Create> => {

  return params;
};

export const update = async (params: T.Update): Promise<T.Update> => {

  return params;
};

export const destroy = async (params: T.Destroy): Promise<T.Destroy> => {

  return params;
};

export const findAndCount = async (params: T.FindAndCount): Promise<T.FindAndCount> => {

  return params;
};