//code generate automatic not edit date: 2023-11-22T00:48:16.194Z
import Adapters from "./adapters";
import { S } from "onbbu/models";
import Models from "./models";

const start = async (props: S.SyncOptions): Promise<void> => {

  await Models.sync(props);

  await Adapters.run();
};

const stop = async (): Promise<void> => {

  await Adapters.stop();
};

export default { start, stop };